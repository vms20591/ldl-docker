# LDL-Docker

This repo contains the necessary **Dockerfile** and **docker-compose** files for various services, to help you get started with LDL.

### Pre-requisites

* GNU/Linux
* docker
* docker-compose
* This repo

----

### Installation

#### Docker

* Install docker from `https://docs.docker.com/install`. Choose your OS from the sidebar menu and follow the instructions
* Check if docker is up and running
    * If user is added to *docker* group - `docker run hello-world`
    * Else - `sudo docker run hello-world`
* If you should get a message like this, then you are good to go!
```
Hello from Docker!
This message shows that your installation appears to be working correctly.
```

#### docker-compose

* Install docker-compose from `https://docs.docker.com/compose/install/`. Choose your OS and follow the instructions

----

### Getting ldl-docker up

* Make sure you have cloned this repo and `cd` into it
```
git clone https://gitlab.com/vms20591/ldl-docker ldl-docker
cd ldl-docker
```
* Run `docker compose up -d`. Wait for the images to be downloaded and start
* If you see something like this, then you are good to go!
```
Creating ldl_calibre-web_1   ... done
Creating ldl_db_1            ... done
Creating ldl_nextcloud_db_1 ... done
Creating ldl_kiwix-web_1    ... done
Creating ldl_redis_1         ... done
Creating ldl_nextcloud_app_1 ... done
Creating ldl_owncloud_web_1  ... done
```
* Use `docker ps` to see currently running containers and some useful info like *container id*, *container name*, *command*, *forwarded ports*, *uptime* & *status*

----

### Host - Container Port Mapping

| **Service**   | **Host Port** | **Container Port**  |
| ------------- |:-------------:| -----:|
| Owncloud      | 8080          | 80    |
| Owncloud      | 8443 (https)  | 443   |
| Nextcloud     | 9080          | 80    |
| Calibre       | 10080         | 80    |
| Kiwix         | 11080         | 8080  |

**Note**: Every service can be accessed from your browser through `localhost:<host_port>`. For Example: To launch **nextcloud** use `http://localhost:9080`, **calibre** use `http://localhost:10080`

----

### Setting the Web Interface

The web interface is simple static page that has link to services like nextcloud, kiwix and calibre. It can be served from any http server or open **index.html** directly in a browser.

* Clone the web interface from `https://gitlab.com/vms20591/LDL-Web-Interface` and `cd` into it
```
git clone https://gitlab.com/vms20591/LDL-Web-Interface LDL-Web-Interface
cd LDL-Web-Interface
```
* Update `index.js` with correct `host` and `port` details specified in **Host - Container Port Mapping** section above.

----

### What is offered from these services via docker?

* Owncloud & Nextcloud - nothing for now, you have to populate your own data 
* Calibre - Bundled with 30 e-books from top 100 books on [Project Gutenber](https://www.gutenberg.org/)
* Kiwix - Bundled with [Wikipedia English Chemistry](http://download.kiwix.org/zim/wikipedia_en_chemistry_nopic.zim) and [Wikipedia English Maths](http://download.kiwix.org/zim/wikipedia_en_maths_nopic.zim) datasets

Hope you all find this useful and helps to get started.